﻿namespace smExample
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbPosResString = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbPosString = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbResString = new System.Windows.Forms.TextBox();
            this.tbValueString = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbPosResDouble = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbPosDouble = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbResDouble = new System.Windows.Forms.TextBox();
            this.tbValueDouble = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbPosRestFloat = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPosFloat = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbResFloat = new System.Windows.Forms.TextBox();
            this.tbValueFloat = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbPosResInt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbPosInt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnReadString = new System.Windows.Forms.Button();
            this.btnReadDouble = new System.Windows.Forms.Button();
            this.btnReadFloat = new System.Windows.Forms.Button();
            this.btnReadInt = new System.Windows.Forms.Button();
            this.btnWriteString = new System.Windows.Forms.Button();
            this.btnWriteDouble = new System.Windows.Forms.Button();
            this.btnWriteFloat = new System.Windows.Forms.Button();
            this.btnOpenString = new System.Windows.Forms.Button();
            this.btnWriteInt = new System.Windows.Forms.Button();
            this.btnOpenDouble = new System.Windows.Forms.Button();
            this.btnOpenFloat = new System.Windows.Forms.Button();
            this.btnOpenInt = new System.Windows.Forms.Button();
            this.tbResInt = new System.Windows.Forms.TextBox();
            this.tbMemString = new System.Windows.Forms.TextBox();
            this.tbMemDouble = new System.Windows.Forms.TextBox();
            this.tbMemFloat = new System.Windows.Forms.TextBox();
            this.tbValueInt = new System.Windows.Forms.TextBox();
            this.tbMemInt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(9, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(341, 432);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbPosResString);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.tbPosString);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.tbResString);
            this.tabPage1.Controls.Add(this.tbValueString);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.tbPosResDouble);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.tbPosDouble);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.tbResDouble);
            this.tabPage1.Controls.Add(this.tbValueDouble);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.tbPosRestFloat);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.tbPosFloat);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.tbResFloat);
            this.tabPage1.Controls.Add(this.tbValueFloat);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.tbPosResInt);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.tbPosInt);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.btnReadString);
            this.tabPage1.Controls.Add(this.btnReadDouble);
            this.tabPage1.Controls.Add(this.btnReadFloat);
            this.tabPage1.Controls.Add(this.btnReadInt);
            this.tabPage1.Controls.Add(this.btnWriteString);
            this.tabPage1.Controls.Add(this.btnWriteDouble);
            this.tabPage1.Controls.Add(this.btnWriteFloat);
            this.tabPage1.Controls.Add(this.btnOpenString);
            this.tabPage1.Controls.Add(this.btnWriteInt);
            this.tabPage1.Controls.Add(this.btnOpenDouble);
            this.tabPage1.Controls.Add(this.btnOpenFloat);
            this.tabPage1.Controls.Add(this.btnOpenInt);
            this.tabPage1.Controls.Add(this.tbResInt);
            this.tabPage1.Controls.Add(this.tbMemString);
            this.tabPage1.Controls.Add(this.tbMemDouble);
            this.tabPage1.Controls.Add(this.tbMemFloat);
            this.tabPage1.Controls.Add(this.tbValueInt);
            this.tabPage1.Controls.Add(this.tbMemInt);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(333, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Access";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tbPosResString
            // 
            this.tbPosResString.Location = new System.Drawing.Point(240, 345);
            this.tbPosResString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosResString.Name = "tbPosResString";
            this.tbPosResString.Size = new System.Drawing.Size(28, 20);
            this.tbPosResString.TabIndex = 41;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(238, 329);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 13);
            this.label22.TabIndex = 40;
            this.label22.Text = "Posit";
            // 
            // tbPosString
            // 
            this.tbPosString.Location = new System.Drawing.Point(132, 345);
            this.tbPosString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosString.Name = "tbPosString";
            this.tbPosString.Size = new System.Drawing.Size(28, 20);
            this.tbPosString.TabIndex = 39;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(130, 329);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 38;
            this.label23.Text = "Posit";
            // 
            // tbResString
            // 
            this.tbResString.Location = new System.Drawing.Point(270, 345);
            this.tbResString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbResString.Name = "tbResString";
            this.tbResString.ReadOnly = true;
            this.tbResString.Size = new System.Drawing.Size(44, 20);
            this.tbResString.TabIndex = 37;
            // 
            // tbValueString
            // 
            this.tbValueString.Location = new System.Drawing.Point(160, 345);
            this.tbValueString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbValueString.Name = "tbValueString";
            this.tbValueString.Size = new System.Drawing.Size(45, 20);
            this.tbValueString.TabIndex = 36;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(276, 329);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Value";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(166, 329);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 34;
            this.label25.Text = "Value";
            // 
            // tbPosResDouble
            // 
            this.tbPosResDouble.Location = new System.Drawing.Point(240, 258);
            this.tbPosResDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosResDouble.Name = "tbPosResDouble";
            this.tbPosResDouble.Size = new System.Drawing.Size(28, 20);
            this.tbPosResDouble.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(238, 242);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Posit";
            // 
            // tbPosDouble
            // 
            this.tbPosDouble.Location = new System.Drawing.Point(132, 258);
            this.tbPosDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosDouble.Name = "tbPosDouble";
            this.tbPosDouble.Size = new System.Drawing.Size(28, 20);
            this.tbPosDouble.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(130, 242);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "Posit";
            // 
            // tbResDouble
            // 
            this.tbResDouble.Location = new System.Drawing.Point(270, 258);
            this.tbResDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbResDouble.Name = "tbResDouble";
            this.tbResDouble.ReadOnly = true;
            this.tbResDouble.Size = new System.Drawing.Size(44, 20);
            this.tbResDouble.TabIndex = 29;
            // 
            // tbValueDouble
            // 
            this.tbValueDouble.Location = new System.Drawing.Point(160, 258);
            this.tbValueDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbValueDouble.Name = "tbValueDouble";
            this.tbValueDouble.Size = new System.Drawing.Size(45, 20);
            this.tbValueDouble.TabIndex = 28;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(276, 242);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 13);
            this.label20.TabIndex = 27;
            this.label20.Text = "Value";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(166, 242);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Value";
            // 
            // tbPosRestFloat
            // 
            this.tbPosRestFloat.Location = new System.Drawing.Point(242, 160);
            this.tbPosRestFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosRestFloat.Name = "tbPosRestFloat";
            this.tbPosRestFloat.Size = new System.Drawing.Size(28, 20);
            this.tbPosRestFloat.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(240, 144);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Posit";
            // 
            // tbPosFloat
            // 
            this.tbPosFloat.Location = new System.Drawing.Point(134, 160);
            this.tbPosFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosFloat.Name = "tbPosFloat";
            this.tbPosFloat.Size = new System.Drawing.Size(28, 20);
            this.tbPosFloat.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(131, 144);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Posit";
            // 
            // tbResFloat
            // 
            this.tbResFloat.Location = new System.Drawing.Point(272, 160);
            this.tbResFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbResFloat.Name = "tbResFloat";
            this.tbResFloat.ReadOnly = true;
            this.tbResFloat.Size = new System.Drawing.Size(44, 20);
            this.tbResFloat.TabIndex = 21;
            // 
            // tbValueFloat
            // 
            this.tbValueFloat.Location = new System.Drawing.Point(162, 160);
            this.tbValueFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbValueFloat.Name = "tbValueFloat";
            this.tbValueFloat.Size = new System.Drawing.Size(45, 20);
            this.tbValueFloat.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(278, 144);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Value";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(167, 144);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Value";
            // 
            // tbPosResInt
            // 
            this.tbPosResInt.Location = new System.Drawing.Point(242, 60);
            this.tbPosResInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosResInt.Name = "tbPosResInt";
            this.tbPosResInt.Size = new System.Drawing.Size(28, 20);
            this.tbPosResInt.TabIndex = 17;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(240, 44);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "Posit";
            // 
            // tbPosInt
            // 
            this.tbPosInt.Location = new System.Drawing.Point(134, 60);
            this.tbPosInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPosInt.Name = "tbPosInt";
            this.tbPosInt.Size = new System.Drawing.Size(28, 20);
            this.tbPosInt.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(131, 44);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Posit";
            // 
            // btnReadString
            // 
            this.btnReadString.Location = new System.Drawing.Point(240, 368);
            this.btnReadString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReadString.Name = "btnReadString";
            this.btnReadString.Size = new System.Drawing.Size(75, 34);
            this.btnReadString.TabIndex = 13;
            this.btnReadString.Text = "Read";
            this.btnReadString.UseVisualStyleBackColor = true;
            this.btnReadString.Click += new System.EventHandler(this.btnReadString_Click);
            // 
            // btnReadDouble
            // 
            this.btnReadDouble.Location = new System.Drawing.Point(240, 281);
            this.btnReadDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReadDouble.Name = "btnReadDouble";
            this.btnReadDouble.Size = new System.Drawing.Size(75, 30);
            this.btnReadDouble.TabIndex = 13;
            this.btnReadDouble.Text = "Leer";
            this.btnReadDouble.UseVisualStyleBackColor = true;
            this.btnReadDouble.Click += new System.EventHandler(this.btnReadDouble_Click);
            // 
            // btnReadFloat
            // 
            this.btnReadFloat.Location = new System.Drawing.Point(240, 183);
            this.btnReadFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReadFloat.Name = "btnReadFloat";
            this.btnReadFloat.Size = new System.Drawing.Size(75, 29);
            this.btnReadFloat.TabIndex = 13;
            this.btnReadFloat.Text = "Read";
            this.btnReadFloat.UseVisualStyleBackColor = true;
            this.btnReadFloat.Click += new System.EventHandler(this.btnReadFloat_Click);
            // 
            // btnReadInt
            // 
            this.btnReadInt.Location = new System.Drawing.Point(240, 84);
            this.btnReadInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReadInt.Name = "btnReadInt";
            this.btnReadInt.Size = new System.Drawing.Size(75, 28);
            this.btnReadInt.TabIndex = 13;
            this.btnReadInt.Text = "Read";
            this.btnReadInt.UseVisualStyleBackColor = true;
            this.btnReadInt.Click += new System.EventHandler(this.btnReadInt_Click);
            // 
            // btnWriteString
            // 
            this.btnWriteString.Location = new System.Drawing.Point(131, 368);
            this.btnWriteString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWriteString.Name = "btnWriteString";
            this.btnWriteString.Size = new System.Drawing.Size(75, 34);
            this.btnWriteString.TabIndex = 12;
            this.btnWriteString.Text = "Write";
            this.btnWriteString.UseVisualStyleBackColor = true;
            this.btnWriteString.Click += new System.EventHandler(this.btnWriteString_Click);
            // 
            // btnWriteDouble
            // 
            this.btnWriteDouble.Location = new System.Drawing.Point(131, 281);
            this.btnWriteDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWriteDouble.Name = "btnWriteDouble";
            this.btnWriteDouble.Size = new System.Drawing.Size(75, 30);
            this.btnWriteDouble.TabIndex = 12;
            this.btnWriteDouble.Text = "Write";
            this.btnWriteDouble.UseVisualStyleBackColor = true;
            this.btnWriteDouble.Click += new System.EventHandler(this.btnWriteDouble_Click);
            // 
            // btnWriteFloat
            // 
            this.btnWriteFloat.Location = new System.Drawing.Point(131, 183);
            this.btnWriteFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWriteFloat.Name = "btnWriteFloat";
            this.btnWriteFloat.Size = new System.Drawing.Size(75, 29);
            this.btnWriteFloat.TabIndex = 12;
            this.btnWriteFloat.Text = "Write";
            this.btnWriteFloat.UseVisualStyleBackColor = true;
            this.btnWriteFloat.Click += new System.EventHandler(this.btnWriteFloat_Click);
            // 
            // btnOpenString
            // 
            this.btnOpenString.Location = new System.Drawing.Point(14, 369);
            this.btnOpenString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpenString.Name = "btnOpenString";
            this.btnOpenString.Size = new System.Drawing.Size(75, 33);
            this.btnOpenString.TabIndex = 11;
            this.btnOpenString.Text = "Open";
            this.btnOpenString.UseVisualStyleBackColor = true;
            this.btnOpenString.Click += new System.EventHandler(this.btnOpenString_Click);
            // 
            // btnWriteInt
            // 
            this.btnWriteInt.Location = new System.Drawing.Point(131, 84);
            this.btnWriteInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWriteInt.Name = "btnWriteInt";
            this.btnWriteInt.Size = new System.Drawing.Size(75, 28);
            this.btnWriteInt.TabIndex = 12;
            this.btnWriteInt.Text = "Write";
            this.btnWriteInt.UseVisualStyleBackColor = true;
            this.btnWriteInt.Click += new System.EventHandler(this.btnWriteInt_Click);
            // 
            // btnOpenDouble
            // 
            this.btnOpenDouble.Location = new System.Drawing.Point(14, 281);
            this.btnOpenDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpenDouble.Name = "btnOpenDouble";
            this.btnOpenDouble.Size = new System.Drawing.Size(75, 30);
            this.btnOpenDouble.TabIndex = 11;
            this.btnOpenDouble.Text = "Open";
            this.btnOpenDouble.UseVisualStyleBackColor = true;
            this.btnOpenDouble.Click += new System.EventHandler(this.btnOpenDouble_Click);
            // 
            // btnOpenFloat
            // 
            this.btnOpenFloat.Location = new System.Drawing.Point(14, 183);
            this.btnOpenFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpenFloat.Name = "btnOpenFloat";
            this.btnOpenFloat.Size = new System.Drawing.Size(75, 29);
            this.btnOpenFloat.TabIndex = 11;
            this.btnOpenFloat.Text = "Open";
            this.btnOpenFloat.UseVisualStyleBackColor = true;
            this.btnOpenFloat.Click += new System.EventHandler(this.btnOpenFloat_Click);
            // 
            // btnOpenInt
            // 
            this.btnOpenInt.Location = new System.Drawing.Point(14, 84);
            this.btnOpenInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpenInt.Name = "btnOpenInt";
            this.btnOpenInt.Size = new System.Drawing.Size(75, 28);
            this.btnOpenInt.TabIndex = 11;
            this.btnOpenInt.Text = "Open";
            this.btnOpenInt.UseVisualStyleBackColor = true;
            this.btnOpenInt.Click += new System.EventHandler(this.btnOpenInt_Click);
            // 
            // tbResInt
            // 
            this.tbResInt.Location = new System.Drawing.Point(272, 60);
            this.tbResInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbResInt.Name = "tbResInt";
            this.tbResInt.ReadOnly = true;
            this.tbResInt.Size = new System.Drawing.Size(44, 20);
            this.tbResInt.TabIndex = 10;
            // 
            // tbMemString
            // 
            this.tbMemString.Location = new System.Drawing.Point(14, 345);
            this.tbMemString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMemString.Name = "tbMemString";
            this.tbMemString.Size = new System.Drawing.Size(76, 20);
            this.tbMemString.TabIndex = 8;
            // 
            // tbMemDouble
            // 
            this.tbMemDouble.Location = new System.Drawing.Point(14, 258);
            this.tbMemDouble.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMemDouble.Name = "tbMemDouble";
            this.tbMemDouble.Size = new System.Drawing.Size(76, 20);
            this.tbMemDouble.TabIndex = 8;
            // 
            // tbMemFloat
            // 
            this.tbMemFloat.Location = new System.Drawing.Point(14, 160);
            this.tbMemFloat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMemFloat.Name = "tbMemFloat";
            this.tbMemFloat.Size = new System.Drawing.Size(76, 20);
            this.tbMemFloat.TabIndex = 8;
            // 
            // tbValueInt
            // 
            this.tbValueInt.Location = new System.Drawing.Point(162, 60);
            this.tbValueInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbValueInt.Name = "tbValueInt";
            this.tbValueInt.Size = new System.Drawing.Size(45, 20);
            this.tbValueInt.TabIndex = 9;
            // 
            // tbMemInt
            // 
            this.tbMemInt.Location = new System.Drawing.Point(14, 61);
            this.tbMemInt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbMemInt.Name = "tbMemInt";
            this.tbMemInt.Size = new System.Drawing.Size(76, 20);
            this.tbMemInt.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 329);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Mem Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 243);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Mem Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(278, 44);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Value";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 144);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Mem Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(167, 44);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Value";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 43);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Mem Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 313);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "String Memory";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 224);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Double Memory";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 126);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Float Memory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Integer Memory";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(74, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Access to shared memories";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 453);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Shared Memory Example";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnReadString;
        private System.Windows.Forms.Button btnReadDouble;
        private System.Windows.Forms.Button btnReadFloat;
        private System.Windows.Forms.Button btnReadInt;
        private System.Windows.Forms.Button btnWriteString;
        private System.Windows.Forms.Button btnWriteDouble;
        private System.Windows.Forms.Button btnWriteFloat;
        private System.Windows.Forms.Button btnOpenString;
        private System.Windows.Forms.Button btnWriteInt;
        private System.Windows.Forms.Button btnOpenDouble;
        private System.Windows.Forms.Button btnOpenFloat;
        private System.Windows.Forms.Button btnOpenInt;
        private System.Windows.Forms.TextBox tbResInt;
        private System.Windows.Forms.TextBox tbMemString;
        private System.Windows.Forms.TextBox tbMemDouble;
        private System.Windows.Forms.TextBox tbMemFloat;
        private System.Windows.Forms.TextBox tbValueInt;
        private System.Windows.Forms.TextBox tbMemInt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPosInt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbPosResInt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbPosRestFloat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPosFloat;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbResFloat;
        private System.Windows.Forms.TextBox tbValueFloat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbPosResString;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbPosString;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbResString;
        private System.Windows.Forms.TextBox tbValueString;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbPosResDouble;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbPosDouble;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbResDouble;
        private System.Windows.Forms.TextBox tbValueDouble;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
    }
}

